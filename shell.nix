{ pkgs ? import (import ./nix/sources.nix { }).nixpkgs { } }:
with pkgs;
mkShell {
  buildInputs = with haskellPackages; [
    ghc
    stack
    ghci
    ghcid
    hlint
    ormolu
    niv
  ];
}
